﻿IF EXISTS (SELECT * FROM sys.databases WHERE Name='Example5')
     DROP DATABASE Example5
GO
CREATE DATABASE Example5
GO
USE Example5
GO
--Tạo bảng lớp học
CREATE TABLE LopHoc (
     MaLopHoc INT PRIMARY KEY IDENTITY,
	 TenLopHoc VARCHAR(10)
)
GO
--Tạo bảng sinh viên có khóa ngoại là cột MaLopHoc, nối với bảng LopHoc
CREATE TABLE SinhVien(
     MaSV int PRIMARY KEY,
	 TenSV varchar(40),
	 MaLopHoc int,
	 CONSTRAINT fk FOREIGN KEY (MaLopHoc) REFERENCES LopHoc (MaLopHoc)
)
GO
--Tạo bảng SanPham với một cột NULL,một cột NOT NULL
CREATE TABLE SanPham (
     Masp int NOT NULL,
	 Tensp varchar (40) NULL
)
GO
--Tạo bnagr với thuộc tính default cho cột Price
CREATE TABLE StoreProduct(
     ProductID int NOT NULL,
	 Name varchar (40) NOT NULL,
	 Price money NOT NULL DEFAULT (100)
)
--Thử kiểm tra xem giá trị dafault có được sử dụng hay không 
INSERT INTO StoreProduct (ProductID, Name) VALUES (111, Rivets)
GO
--Tạo bảng ContactPhone với thuộc tính IDETITY
CREATE TABLE ContactPhone (
     Person_ID int IDENTITY (500,1) NOT NULL,
	 MobileNumber bigint NOT NULL
)
GO
--Tạo cột nhận dạng duy nhất tổng thể
CREATE TABLE CellularPhone (
     Person_ID uniqueidentifier DEFAULT NEWID() NOT NULL,
	 PersonName varchar (60) NOT NULL
)
--Chèn một record vào
INSERT INTO CellularPhone(personName) VALUES ('William Smith')
GO
--Kiểm tra giá trị của cột Person_ID tự động sinh
CREATE TABLE ContactPhone (
     Person_ID int PRIMARY KEY,
	 MobileNumber bigint UNIQUE,
	 ServiceProvider varchar(30),
	 LandlineNumber bigint UNIQUE
)
--Chèn 2 bản ghi có giá trị giống nhau ở cột MobileNumber và LanlieNumber để quan sát lỗi
INSERT INTO ContactPhone values (101, 983345674, 'Hutch',NULL)
INSERT INTO ContactPhone values (102, 983345674, 'Alex', Null)
GO
--Tạo bảng PhoneExpenses có một CHECT ở cột Amount
CREATE TABLE PhoneExpenses (
     Expense_ID int PRIMARY KEY,
	 MobileNumber bigint FOREIGN KEY REFERENCES  ContactPhone
(MobileNumber),
GO
--Chỉnh sữa cột trong bảng
ALTER TABLE ContactPhone
    ALTER COLUMN ServiceProvider varchar(45)
GO
--Xóa cột trong bảng 
ALTER TABLE ContractPhone
    ALTER COLUMN ServiceProvider varchar(45)
GO
--Xóa cột trong bảng
ALTER TABLE ContactPhone
     DROP COLUMN ServiceProvider
GO
---Thêm một rằng buộc vào bảng
ALTER TABLE ContactPhone ADD CONSTRAINT CHK_RC CHECK (RentalCharges >0)
  GO
  --Xóa một ràng buộc
  ALTER TABLE Person.ContactPhone
      DROP CONSTRAINT CHK_RC
