alter table ACCOUNT
add constraint ACCOUNT_CUSTOMER_FK
foreign key (CUST_ID)
references CUSTOMER;

alter table ACCOUNT
add constraint ACCOUNT_BRANCH_FK
foreign key (OPEN_BRANCH_ID)
references BRANCH;

alter table ACCOUNT
add constraint ACCOUNT_EMPLOYEE_FK
foreign key (OPEN_EMP_ID)
references EMPLOYEE;

alter table ACCOUNT
add constraint ACCOUNT_PRODUCT_FK
foreign key (PRODUCT_CD)
references PRODUCT;

alter table ACC_TRANSACTION
add constraint ACC_TRANSACTION_ACCOUNT_FK
foreign key (ACCOUNT_ID)
references ACCOUNT;

alter table ACC_TRANSACTION
add constraint ACC_TRANSACTION_BRANCH_FK
foreign key (EXECUTION_BRANCH_ID)
references BRANCH;

alter table ACC_TRANSACTION
add constraint ACC_TRANSACTION_EMPLOYEE_FK
foreign key (TELLER_EMP_ID)
references EMPLOYEE;

alter table BUSINESS
add constraint BUSINESS_EMPLOYEE_FK
foreign key (CUST_ID)
references CUSTOMER;

alter table EMPLOYEE
add constraint EMPLOYEE_BRANCH_FK
foreign key (ASSIGNED_BRANCH_ID)
references BRANCH;

alter table EMPLOYEE
add constraint EMPLOYEE_DEPARTMENT_FK
foreign key (DEPT_ID)
references DEPARTMENT;

alter table EMPLOYEE
add constraint EMPLOYEE_EMPLOYEE_FK
foreign key (SUPERIOR_EMP_ID)
references EMPLOYEE;

alter table INDIVIDUAL
add constraint INDIVIDUAL_CUSTOMER_FK
foreign key (CUST_ID)
references CUSTOMER;

alter table OFFICER
add constraint OFFICER_CUSTOMER_FK
foreign key (CUST_ID)
references CUSTOMER;

alter table PRODUCT
add constraint PRODUCT_PRODUCT_TYPE_FK
foreign key (PRODUCT_TYPE_CD)
references PRODUCT_TYPE;
